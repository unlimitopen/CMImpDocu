# CMImpDocu
Scanning import Documents for bills and information into the CMImpDocu. This project was startet because we want to scan bills and informations in paper format into a Postgres sql database or anything else database. The bills or the other information in paper format must be stored by law and in some cases the time to store the information is not really clearly!
The best example is the tax return in germany, in some cases you need for the pension your tax return for over 35 years, ok that is not the standard either, but if you have the information you can really happy about it. In some other cases it is really easy to search the scanned documents.
For your notice this is a renew of a old development of CMImpDocu, in 2014 we has began a development for this function, but we never finished it. In the first version we used cuniform to make the text recognition it works very well.

----

#### >>>> Before you start <<<<

Before you download the code, please know the informations about the versions.
With this information you have to known: how to install the impDocu.

----

At this moment: 2017-12-14 - the impdocu is not ready for using.

----

#### Technical Informations
At this acutal development we use the sane (scanimage) software on linux systems to scan the documents from a printer directly. After the scan job is ready, a text recognition with Googles "tesseract" will be executed and will store the reachable text as meta information into the database. 
Now we come to the internal doing:
For indexing the documents we will put on the first site of document a qr-code, the qr code is in the first part to index the document and seperate the different documents, in the second part you can use this qr code to write it in the outsite on a carton, because some users cannot the original scanned documents throw away. You can print  directly a count of qr-code on one plain paper and put this codes between the seperate documents or you can tell the programm where the qr code needs to be saved between the different documents.

Ok, this text is really difficult to understand, but it is really a difficult subject.
In the next versions, we will insert a function that ask you on how document site the qr-code must be insert and then the program will manipulate the document or will put picture document site with a qr-code between the defined sites.

##### So let me try it how does the documents will be scanned with qr-code:
qr-code site with qr-code number: 0000001 (printed through CMImpDocu)  
bill document site 1  
bill document site 2  
qr-code site with qr-code number: 0000002 (printed through CMImpDocu)  
information document 1  
qr-code site with qr-code number: 0000003 (printed through CMImpDocu)  
bill document site 1  
bill document site 2  
bill document site 3  
qr-code site with qr-code number: 0000004 (printed through CMImpDocu)  
information document 1  
information document 2  

Do you understand now what i'm talking about?

#### System dependencies
* intel or arm based system (Raspberry Pi 2 or BananaPI M2)
* linux system, debian jessie or ubuntu 16.04
* sane installed on the linux system
* tesseract installed on the linux system
* QR-code tools installed on the linux system
* a lot of free space  for the scanned documents
* hp officejet 8600 printer or other hp multifunction printer

----

#### Architecture
View of a single architecture:
![ This is a view of a single architecture ](/doc/single-architecture.png)

View API-nfrastructure: (__not finished yet, but thats is the way we want to go with the development__)
![ This is a view of a multi architecture ](/doc/api_architecture.png)

View of a parsing architecture: (__not complete finished yet__)
![ This is a view of a multi architecture ](/doc/internal_parsing_architecture.png)

View of a multi architecture: (__not finished yet, but thats is the way we want to go with the development__)
![ This is a view of a multi architecture ](/doc/multi-architecture.png)

----

#### Known issues / Known Requests
| issue / discription | answer / comment | open / close / request | version fixed |
| -------- | -------- | -------- | -------- |
| din a4 and letter paper format will not support if they will scanned in on step. | Yes, it is not directly our problem, because the printer ADF cannot handle this, but your right we have not implement a check if there is a a4 or letter format document. | request | > 0.1.1 |
| you dont support duplex adf scans | thats not correct we support that, but your printer need this feature to use that. If you see that duplex is marked gray, in your scan settings, your printer has not tell us that support duplex scans in adf. | close | > 0.0.5 |
| the impdocu will save all scans in tiff format, that is a problem that my harddisk is growed up my disc space very fast! | yes, we now that, but tif is actual the best format that we can use for text recognition, but we will implement a other constellation. | request | > 0.0.9 |

----

#### Version and History
| date | developer | version | answer / comment | actual version online |
| -------- | -------- | -------- | -------- | -------- |
| 05.10.2017 | c.möllers | 0.0.1 | initial release |  |
| 05.10.2017 | c.möllers | 0.0.2 | make module for scanimage / sane to control the scanner functions |  |
| 08.10.2017 | c.möllers | 0.0.3 | insert the ocr recognition with tesseract |  |
| 08.10.2017 | c.möllers | 0.0.4 | implement the qr code control to print banner pages |  |
| 09.10.2017 | c.möllers | 0.0.5 | insert a function they will put the seperatet documents togehter. | 
| 23.10.2017 | c.möllers | 0.0.6 | we insert a indexing function for file indexing. |  |
| 25.10.2017 | c.möllers | 0.0.6 | start a json index for the file indexing. |
| 26.10.2017 | c.möllers | 0.0.7 | Implement for the first step a postgresql support. | 
| 26.10.2017 | c.möllers | 0.0.7 | make a interactive flow to display question. | 
| 03.11.2017 | c.möllers | 0.0.8 | install a file search to make updates for file indexing. |  
| 04.11.2017 | c.möllers | 0.0.8 | change the internal handling for indexing documents. |
| 05.11.2017 | c.möllers | 0.0.8 | insert a function to change the ocr recognition programm. We want to use tesseract or cuneform.|  
|  |  |  |  |  |
| 25.01.2018 | c.möllers | 1.0.0 | documentation and help with cli. | 0.1.0 |
| 31.01.2018 | c.möllers | 0.1.0 | -------------------------- |  |
| 20.02.2018 | c.möllers | 0.1.0 | first installable release. | 0.1.0 (only command line interface) |

#### Which multifunction printers are supported
All multifunction printers are supported that sane can handle, some printers needed to be connected with usb port or some printers are possible to communicate with http / https. (the second protocol is prefered!)
In this actual version we can say this printers can be used:  

epson workforce 3620 (development)  
hp officejet 8600  
hp officejet 6500  

We will support and use many more multifunction printers, please give us a little bit time to investigate!
