#
# -*- coding: utf-8 -*-

# ImportantDocuments (impDocu)

import os
import subprocess
# import spidev
import time 
#import serial
import sys
#from queue import Queue
import threading
import re
import json
import glob
import hashlib
from datetime import datetime
# self made modules
sys.path.append("../mod/")

from CMCore import CMCore

class impDocu:
    '''
    Import(ant) Documents

    import your own very important documents into a database.
    why? because in some situations you search for a very important document
    on your files but it is all analog and you need to read over many papers.
    you can lose your mind or your lose your calm, thats is why we have build
    this program.
    '''

    workingDirectories = ['../qra/', '../log/', '../arc/', '../tmp/', '../inc/', '../cfg/']

    timeNow = ""
    filetime = ""
    scanFileExtensions= "tif"
    incDirectory = "../inc/"
    tmpDirectory = "../tmp/"
    archiveDirectory = "../arc/"

    # important module import
    # and definition
    coreHandle = CMCore()


    def __init__(self):
        '''

        '''
        self.timeNow = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        self.fileTime = self.coreHandle.timeHandle.getDateFromTimeAndDate(self.timeNow)
        self.workingDirectories[0] += self.fileTime
        self.workingDirectories[2] += self.fileTime
        self.workingDirectories[4] += self.fileTime


    def doQueryForImportDocumentsCLI(self, fileList):
        '''

        :param fileList:
        :return:
        '''



    def doQrCodesForScannedDocuments(self, qrCount=10):
        '''

        :param qrCount:
        :return:
        '''

        #self.coreHandle.qrCodes.makeQRCodesFromDefaultWithCount(1, 2, qrCount)

        # sys.exit()

        # self.scanImage.scanImageGetPathFromExecutable()
        # self.scanImage.getScanImage()
        # self.convertImages.convertAllFilesInDirectory()

        # fileListe['456'] = [22,33,44,55]
        # fileListe['222'] = [11,99,66,77]
        # print (fileListe)
        # print(len(fileListe['222']))
        # sys.exit()

        # self.coreHandle.loggerHandle.messages("i","Search a little test")

        # self.loggerHandle.messages("i", "Make qr codes!")
        self.coreHandle.psHandle.makePagesWithQRCode()

        # self.loggerHandle.messages("d", "Get scanner options")
        # self.scanImage.scanImageGetDeviceOptions()

        # self.loggerHandle.messages("d","Scan images from scanner")
        # self.scanImage.getScanImage()

        # self.loggerHandle.messages("d","Convert all files from directory")
        # self.convertImages.convertAllFilesInDirectory()

        # fileDict = {'20171126000003_IPDC': {'documentInsertTime': None, 'documentOwnerPos': 16, 'documentCompanyPos': 17, 'documentDate': ['01.08.2017'], 'documentLastModifyTime': '17:03:33', 'documentInsertDate': None, 'documentLastModifyDate': '2018-01-03', 'documentDatePos': 13, 'documentOwner': ['herr weiligmann'], 'tmpDocuments': ['../tmp/2018-01-03_20171126000003_IPDC_FILE_0', '../tmp/2018-01-03_20171126000003_IPDC_FILE_1'], 'documentPages': 2, 'picDocuments': ['../inc/37.tif', '../inc/38.tif'], 'documentSize': 514502, 'documentCreationDate': '2018-01-03', 'documentType': ['umschaltung im netz'], 'documentCompany': ['telekom'], 'documentName': '2018-01-03_20171126000003_IPDC_FILE.pdf', 'documentCreationTime': '17:03:33', 'documentLink': '../arc/2018-01-03_20171126000003_IPDC_FILE.pdf', 'document_id': None, 'documentTypePos': 15}}
        # newFile = self.coreHandle.postgresHandle.insertIndexingInformationIntoDatabase(fileDict)
        # print(newFile)
        # sys.exit()

        # informations = self.coreHandle.postgresHandle.getAllIndexingInformationFromDatabase()
        # print(len(informations))
        # metadata = informations[0]

        # print(metadata)

        # self.coreHandle.parserHandle.getOwnerIdentification('../tmp/2018-01-03_20171126000013_IPDC_FILE_0.txt')

        # sys.exit()


    def makeFileCheckFromImportedDocuments(self):
        '''

        :return:
        '''

        success = False
        scoreTable = 0

        self.coreHandle.loggerHandle.messages("d", "Get all imported documents from database and check if there exists")
        self.coreHandle.fileDict = self.coreHandle.postgresHandle.getAllImportedFilesFromDatabase()

        if (self.coreHandle.fileDict):
            self.coreHandle.loggerHandle.messages("i", "Files where found in the database.")

            for file in self.coreHandle.fileDict.keys():

                fileExist = self.coreHandle.fileHandle.getFileExists(self.coreHandle.fileDict[file]['documentLink'])
                if (fileExist == True):
                    scoreTable = 1
                    self.coreHandle.fileDict[file].update({'documentCheckExists': True})
                else:
                    scoreTable = -1
                    self.coreHandle.fileDict[file].update({'documentCheckExists': False})

                md5sum = self.coreHandle.fileHandle.getMD5ValueFromFile(self.coreHandle.fileDict[file]['documentLink'])
                if (md5sum == self.coreHandle.fileDict[file]['documentMD5Sum']):
                    scoreTable += 1
                    self.coreHandle.fileDict[file].update({'documentCheckMD5Sum': True})
                else:
                    scoreTable -= 1
                    self.coreHandle.fileDict[file].update({'documentCheckMD5Sum': False})

                documentSize = self.coreHandle.fileHandle.getFileSizeFromFile(self.coreHandle.fileDict[file]['documentLink'])
                if (documentSize == self.coreHandle.fileDict[file]['documentSize']):
                    scoreTable += 1
                    self.coreHandle.fileDict[file].update({'documentCheckSize': True})
                else:
                    scoreTable -= 1
                    self.coreHandle.fileDict[file].update({'documentCheckSize': False})

                documentPages = self.coreHandle.gsHandle.getTotalPagesFromPDFDocument(self.coreHandle.fileDict[file]['documentLink'])

                if (documentPages == self.coreHandle.fileDict[file]['documentPages']):
                    scoreTable += 1
                    self.coreHandle.fileDict[file].update({'documentCheckPages': True})
                else:
                    scoreTable -= 1
                    self.coreHandle.fileDict[file].update({'documentCheckPages': False})

                documentFileCreation = self.coreHandle.fileHandle.getCreationDateFromFile(self.coreHandle.fileDict[file]['documentLink'])
                documentFileCreation = self.coreHandle.timeHandle.getDateFromTimeTicks(documentFileCreation * 1000)
                documentFileCreation = datetime.strptime(documentFileCreation, '%Y-%m-%d %H:%M:%S')
                documentFileCreationFromTable = datetime.combine(self.coreHandle.fileDict[file]['documentCreationDate'], \
                                                                 self.coreHandle.fileDict[file]['documentCreationTime'])

                if (documentFileCreation == documentFileCreationFromTable):
                    scoreTable += 1
                    self.coreHandle.fileDict[file].update({'documentCheckCreation': True})
                else:
                    scoreTable -= 1
                    self.coreHandle.fileDict[file].update({'documentCheckCreation': False})

                self.coreHandle.loggerHandle.messages("i", "Scoretable for file: {} is: {}.".format(file, scoreTable))

                if (scoreTable == 5):
                    self.coreHandle.loggerHandle.messages("i", "Document is correct and will updated in the database")
                    self.coreHandle.fileDict[file].update({'documentStatus': True})
                else:

                    self.coreHandle.loggerHandle.messages("i",
                                               "Document is incorrect and we need to correct the \n file:{} \n document database id:{} \n document exists:{} \n document md5 sum:{}, \n document size:{}, \n document pages:{}, \n document creation time:{}".format(file,
                                                        self.coreHandle.fileDict[file]['document_id'],
                                                        self.coreHandle.fileDict[file]['documentCheckExists'],
                                                        self.coreHandle.fileDict[file]['documentCheckMD5Sum'],
                                                        self.coreHandle.fileDict[file]['documentCheckSize'],
                                                        self.coreHandle.fileDict[file]['documentCheckPages'],
                                                        self.coreHandle.fileDict[file]['documentCheckCreation'],))
                    self.coreHandle.fileDict[file].update({'documentStatus': False})

            success = self.coreHandle.postgresHandle.updateAllImportedFilesFromDatabase(self.coreHandle.fileDict)

            if (success == True):
                self.coreHandle.loggerHandle.messages("i", "Updates for file in archive are ready")
            else:
                self.coreHandle.loggerHandle.messages("e", "Updates for file in archive goes wrong please read log file below!")

        else:
            self.coreHandle.loggerHandle.messages("i", "No files where found in the database.")



    def getQRCodesFromScannedDocuments(self):
        '''

        :return:
        '''

        fileListe = None

        self.coreHandle.loggerHandle.messages("i", "Reading the files from incoming directory")
        fileListe = self.coreHandle.fileHandle.getFilesFromDirectoryWithExtension(self.incDirectory,self.scanFileExtensions)

        if (len(fileListe) >= 1):
            self.coreHandle.loggerHandle.messages("i", "Reading the qrcodes from scanned file list")
            fileListe = self.coreHandle.qrCodes.getQRCodesFromDirectory(fileListe)
        else:
            fileListe = None

        return fileListe


    def doSearchForScannedFiles(self):
        '''
        
        :return: 
        '''
        success = False

        fileListe = self.getQRCodesFromScannedDocuments()
        if (fileListe != None):
            #
            # here we need to sort lists with qr codes and without qr codes.
            # and the filelist with qr codes will be indexed into the database!
            #
            counter = 0
            for qrCodeFile in fileListe:
                if (qrCodeFile == None):
                    for file in fileListe[qrCodeFile]:
                        while(True):
                            self.coreHandle.loggerHandle.messages("p", "Now we open a pic viewer for scanned documents:")
                            success = self.coreHandle.viewHandle.openFileWithLocalViewer(file)
                            self.coreHandle.loggerHandle.messages("p", "If you want a barcode before the document?")
                            question = input()
                            question = question.lower()
                            if (question == 'y' or question == 'j'):
                                counter = 2
                                self.coreHandle.loggerHandle.messages("p", "Ok, we put a barcode before the document.")
                                self.coreHandle.loggerHandle.messages("p", "please wait.....")
                                success = self.coreHandle.qrCodes.makeQRCodeBeforeImportantDocument(file)
                                newFilename = file
                                newending = "_" + str(counter) + ".tif"
                                newFilename = newFilename.replace('.tif', newending)
                                self.coreHandle.fileHandle.moveFileFromTo(file, newFilename)
                                break

                            elif (question == 'n'):
                                counter += 1
                                self.coreHandle.loggerHandle.messages("p", "Ok, lets view the next picture.")
                                newFilename = file
                                newending = "_" + str(counter) +".tif"
                                newFilename = newFilename.replace('.tif', newending)
                                self.coreHandle.fileHandle.moveFileFromTo(file, newFilename)
                                break

                            else:
                                self.coreHandle.loggerHandle.messages("p", "We not understand your info: {}.".format(question))

            self.coreHandle.loggerHandle.messages("i", "Reading the files from incoming directory")
            fileListe = self.coreHandle.fileHandle.getFilesFromDirectoryWithExtension(self.incDirectory,self.scanFileExtensions)

            if (len(fileListe) >= 1):
                self.coreHandle.loggerHandle.messages("i", "Reading the qrcodes from scanned file list")
                fileListe = self.coreHandle.qrCodes.getQRCodesFromDirectory(fileListe)

                self.doImportDocuments()

        else:
            self.coreHandle.loggerHandle.messages("i", "We have no files to indexing and put into database")

        return success


    def doImportDocuments(self):
        '''

        :param fileDict:
        :return:
        '''
        fileListe = self.getQRCodesFromScannedDocuments()
        if (fileListe != None):
            for qrCode in fileListe.keys():
                self.coreHandle.fileDict[qrCode] = {'picDocuments': fileListe[qrCode]}
                self.coreHandle.fileDict[qrCode].update({'documentName': None})
                self.coreHandle.fileDict[qrCode].update({'documentLink': None})
                self.coreHandle.fileDict[qrCode].update({'documentStatus': None})
                self.coreHandle.fileDict[qrCode].update({'documentPages': None})
                self.coreHandle.fileDict[qrCode].update({'documentSize': None})
                self.coreHandle.fileDict[qrCode].update({'documentLastModifyDate': None})
                self.coreHandle.fileDict[qrCode].update({'documentLastModifyTime': None})
                self.coreHandle.fileDict[qrCode].update({'tmpDocuments': None})
                self.coreHandle.fileDict[qrCode].update({'documentOwner': None})
                self.coreHandle.fileDict[qrCode].update({'documentCompany': None})
                self.coreHandle.fileDict[qrCode].update({'documentType': None})
                self.coreHandle.fileDict[qrCode].update({'documentDate': None})
                self.coreHandle.fileDict[qrCode].update({'document_id': None})
                self.coreHandle.fileDict[qrCode].update({'documentMD5Sum': None})
                self.coreHandle.fileDict[qrCode].update({'documentImportDate': None})
                self.coreHandle.fileDict[qrCode].update({'documentImportTime': None})
                self.coreHandle.fileDict[qrCode].update({'documentIndexReady': False})

            self.coreHandle.loggerHandle.messages("i", "Make ocr from scanned files")
            self.coreHandle.fileDict = self.coreHandle.ocrTesseract.makeTesseractThreads(self.coreHandle.fileDict,
                                                                                         self.fileTime,
                                                                                         self.tmpDirectory)

            self.coreHandle.loggerHandle.messages("i", "Parse the text files to write into db")

            # we need here the dictionary with file informations
            self.coreHandle.loggerHandle.messages("i", "Get the information from the scanned documents.")
            self.coreHandle.fileDict = self.coreHandle.parser.getIdentificationsFromDocumentPath(
                self.coreHandle.fileDict)

            #
            self.coreHandle.loggerHandle.messages("i", "Merge pdf file into one pdf")
            self.coreHandle.fileDict = self.coreHandle.gsHandle.makeGsHandleToMerge(self.coreHandle.fileDict,
                                                                                    self.archiveDirectory,
                                                                                    self.fileTime)
            #
            # self.coreHandle.checkFileDict(self.coreHandle.fileDict)

            # get the md5 value from file
            self.coreHandle.fileDict = self.coreHandle.fileHandle.getMD5ValueFromFileList(self.coreHandle.fileDict)

            # insert the information into DB!
            self.coreHandle.loggerHandle.messages("i", "Insert the indexing informations into the database")
            self.coreHandle.fileDict = self.coreHandle.postgresHandle.insertIndexingInformationIntoDatabase(self.coreHandle.fileDict)

            #self.coreHandle.loggerHandle.messages("i", "Insert the indexing informations into the important documents")
            #self.coreHandle.fileDict = self.coreHandle.fileHandle.insertIndexingInformationIntoFileHeader(self.coreHandle.fileDict)
            # self.coreHandle.fileDict = self.coreHandle.fileHandle.readIndexingInformationFromFileHeader(self.coreHandle.fileDict[file]['pdfDocumentLink'])

    def getTheScanImages(self):
        '''

        :return:
        '''

        self.coreHandle.loggerHandle.messages("d","Scan images from scanner")
        self.coreHandle.scanImage.getScanImage()


    def doHelp(self):
        '''

        :return:
        '''

        usage ="""Program options for {}:
-sps or --searchpictures        = identify the scanned documents
-ids or --importdocuments       = import scanned documents
-mgr or --makeqrcodes           = make qrcodes
-scd or --scandocuments         = scan documents from scanner
               
Options:
-c or --count                   = counter for some options
-----------------------------------------------------------------
-h or --help                    = print out this help text """.format(__file__)
        self.coreHandle.loggerHandle.messages("i", "Printing help text")
        print(usage)


    def doRun(self, argv):
        '''

        :param sysargv:
        :return:
        '''
        optList = []

        optArgs = {
            "-c":               [2,2],
            "-fc":              [1,1],
            "-sps":             [1,1],
            "-ids":             [1,1],
            "-mqr":             [1,1],
            "-scd":             [1,1],
            "-h":               [1,1],

            "--count":          [2,2],
            "--filecheck":      [1,1],
            "--searchpictures": [1,1],
            "--importdcouments": [1,1],
            "--makeqrcodes":    [1,1],
            "--scandocuments":  [1,1],
            "--help":           [1,1],
        }

        for arg in argv:
            if(arg in optArgs.keys()):
                optList.append(arg.replace("-","",2))

                #if (re.findall("c=")):
                continue

        if (len(optList)<=0):
            self.doHelp()
            sys.exit()
        else:


            if ('ids' in optList or 'importdocuments' in optList):
                self.doImportDocuments()

            if ('sps' in optList or 'searchpictures' in optList):
                self.doSearchForScannedFiles()

            if ('fc' in optList or 'filecheck' in optList):
                self.makeFileCheckFromImportedDocuments()



if __name__ == '__main__':
    check = impDocu()

    check.coreHandle.fileHandle.createFoldersFromList(check.workingDirectories)
    check.coreHandle.fileHandle.removeAllFilesFromDirectory(check.tmpDirectory)
    check.coreHandle.fileHandle.removeAllFilesFromDirectory(check.coreHandle.qrCodes.getQRPath())
    #check.coreHandle.fileHandle.moveAllFilesToDateDirectory(check.workingDirectories, check.fileTime)
    check.doRun(sys.argv[1:])
    #check.coreHandle.fileHandle.moveAllFilesToDateDirectory(check.workingDirectories, check.fileTime)
    check.coreHandle.fileHandle.removeAllFilesFromDirectory(check.tmpDirectory)
    check.coreHandle.fileHandle.removeAllFilesFromDirectory(check.coreHandle.qrCodes.getQRPath())
    check.coreHandle.fileHandle.moveAllFilesToDateDirectory(check.workingDirectories, check.timeNow)
    check.coreHandle.fileHandle.checkDirectoryIsEmptyAndRemove(['../qra/', '../log/', '../arc/', '../inc/', '../cfg/'])

