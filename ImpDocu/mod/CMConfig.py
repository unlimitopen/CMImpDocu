#
# -*- coding: utf-8 -*-

import os
import subprocess
import time
import sys
#from queue import Queue
import threading
import json

# self made modules
sys.path.append("../mod/")

from CMLogger import CMLogger

class CMConfig():
    '''

    '''
    logFile = None
    loggerHandle = CMLogger()


    def setLogFile(self, logfile):
        '''

        :param logfile:
        :return:
        '''

        self.logFile = logfile
        self.loggerHandle.setLogFile(self.logFile)
