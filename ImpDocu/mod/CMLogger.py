# -*- coding: utf-8 -*-
import os
import time
import sys

class CMLogger():
    '''

    '''
    pid = os.getpid()
    logfile = ""
    logLevel = 0
    logFileMaxSize = 20000
    logLevelSetup = 6


    def __init__(self):
        '''
        
        '''


    def getLogLevel(self):
        '''
        
        :return: 
        '''
        return self.logLevel

    def setLogLevel(self, logLevel):
        '''
        
        :param logLevel: 
        :return: 
        '''
        self.logLevel = logLevel


    def getLogFile(self):
        '''
        
        :return: 
        '''
        return self.logfile



    def setLogFile(self, logfile):
        '''
        
        :param logfile: 
        :return: 
        '''

        self.logfile = logfile



    def messages(self, level, message):
        '''
        
        :param level: 
        :param message: 
        :return: 
        '''

        self.writeToLog(level, message)



    def writeToLog(self, message_info, message_text):
        '''
        Write message to logfile
        '''

        # set logging information
        # logging deactivated = 0
        # info logging        = 1
        # warn logging        = 2
        # error logging       = 3
        # critical logging    = 4
        # debug logging       = 5
        # trace logging       = 6

        message_typ = "[ERROR]"
        if message_info.lower() == "i" and self.logLevelSetup >= 1:
            message_typ = "[INFO]"
        if message_info.lower() == "n":
            message_typ = "[INSERT]"
        if message_info.lower() == "d" and self.logLevelSetup >= 5:
            message_typ = "[DEBUG]"
        if message_info.lower() == "w" and self.logLevelSetup >= 2:
            message_typ = "[WARN]"
        if message_info.lower() == "e" and self.logLevelSetup >= 3:
            message_typ = "[ERROR]"
        if message_info.lower() == "t" and self.logLevelSetup >= 6:
            message_typ = "[TRACE]"
        if message_info.lower() == "p":
            message_typ = "[STDOUT]"
        if message_info.lower() == "c" and self.logLevelSetup >= 4:
            message_typ = "[CRITICAL]"

        # get the local time from system
        aktime = time.strftime("%d.%m.%Y %H:%M:%S")

        # the stdout prints write not down into logfile
        if (message_info.lower() == "p"):
            print("[{}] {} {} {} ".format(self.pid,aktime,message_typ,message_text))

        try:
            if os.path.exists(self.logfile) == True:
                fobj = open(self.logfile, "a")
            else:
                fobj = open(self.logfile, "w")

            # print (fobj, "[", self.pid, "]", aktime, message_typ, message_text)
            s = "[" + str(self.pid) + "]" + " " + aktime + " " + message_typ + " " + message_text + "\n"
            print(s, end="", file=fobj)
            fobj.close

        except IOError as err:
            print(err)

        except Exception as err:
            print(err)