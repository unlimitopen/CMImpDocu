########! /usr/bin/python
#
# -*- coding: utf-8 -*-

import os
import subprocess
import time
import sys
#from queue import Queue
import threading
import re
import json
import glob
from PIL import Image
import pyqrcode
import zbarlight
import sane
import numpy

# self made modules
sys.path.append("../mod/")

from CMFileHandle import CMFileHandle
from CMTimeHandle import CMTimeHandle

class CMQRCode():
    '''

    '''
    # define a instance
    fileHandle = CMFileHandle()
    timeHandle = CMTimeHandle()

    timeNow = timeHandle.getTimeAndDate()
    date = timeHandle.getDateFromTimeAndDate(timeNow)

    qrCodeOnDocuments = 0  # if the is 1 than qrcode is glued on document
    qrCodeSize = 3
    qrCodeDefaultName = "0000001_IPDC"
    qrCodeBeginNumber = 15
    qrCodeDefaultbegin = date
    qrCodeDefaultExtension = "_IPDC"
    qrCodeDirectoryPath = "../qra/"
    qrCodeCounter = 5

    def __init__(self):
        '''

        '''
        pass


    def setQRCounter(self, qrCounter):
        '''

        :param qrCounter:
        :return:
        '''
        self.qrCodeCounter = qrCounter


    def getQRCounter(self):
        '''

        :return:
        '''
        return self.qrCodeCounter


    def setQRDefaultName(self, string):
        '''

        :param string:
        :return:
        '''
        self.qrCodeDefaultName = string


    def setQRPath(self, directory):
        '''

        :param directory:
        :return:
        '''

        self.qrCodeDirectoryPath = directory


    def getQRPath(self):
        '''

        :return:
        '''

        return self.qrCodeDirectoryPath


    def setQRBeginNumber(self, beginNumber):
        '''

        :return:
        '''
        self.qrCodeBeginNumber = beginNumber


    def getQRBeginNumber(self):
        '''

        :return:
        '''

        return self.qrCodeBeginNumber


    def setQRCodeSize(self, qrCodeSize):
        '''

        :param qrCodeSize:
        :return:
        '''
        self.qrCodeSize = qrCodeSize


    def getQRCodeSize(self):
        '''

        :return:
        '''
        return self.qrCodeSize


    def makeQRCodesFromDefaultWithCount(self, qrNumber, size=3, count=1):
        '''

        :param filname:
        :param string:
        :param size:
        :param count:
        :return:
        '''
        fileCount = 1
        qrCodeList = []

        qrCount, qrName = self.qrCodeDefaultName.split("_")

        for file in range(1,count):
            if (file <= 9):
                filename = "00000" +str(qrNumber) + "_" + qrName
            elif (file > 9 and file <= 99):
                filename = "0000"+str(qrNumber) + "_" + qrName
            elif (file <= 999 and file <= 99 ):
                filename = "000"+str(qrNumber) + "_" + qrName
            elif (file <= 9999 and file <= 999):
                filename = "00"+str(qrNumber) + "_" + qrName
            elif (file <= 99999 and file <= 9999):
                filename = "0"+str(qrNumber) + "_" + qrName
            elif (file <= 999999 and file <= 99999):
                filename = str(qrNumber) + "_" + qrName

            filename = self.date.replace('-','') + filename
            self.makeQRCodes(filename, filename, size)
            fileCount += 1
            qrNumber += 1

        if (count == fileCount):
            qrCodeList = self.fileHandle.getAllFilesFromDirectory(self.qrCodeDirectoryPath)
        else:
            qrCodeList = None

        return qrCodeList


    def makeQRCodes(self, filename, string, size=3):
        '''

        :param filename:
        :param string:
        :param size:
        :return:
        '''
        filename = self.qrCodeDirectoryPath+"/"+filename+".png"
        try:
            qr = pyqrcode.create(string)
            qr.png(filename, scale=size)

        except Exception as error:
            pass



    def readQRCodes(self, filename, scanCode='qrcode'):
        '''
        read from filename the QR-Codes
        :param filename:
        :return:
        '''
        code = None
        success = False

        try:
            with open(filename, 'rb') as image_file:
                image = Image.open(image_file)
                image.load()
            code = zbarlight.scan_codes(scanCode, image)

        except Exception as error:
            codes = None
            success = False

        return code


    def getQRCodesFromDirectory(self, directory, fileExtension="*.tif"):
        '''

        :param fileList:
        :return:
        '''
        oldQrCode = None
        newList = []
        fileListe = {}

        allFiles = glob.glob(directory + fileExtension)
        allFiles.sort(key=self.fileHandle.sortKeyFunc)

        for file in allFiles:
            qrCodeSingle = self.readQRCodes(file)
            if (qrCodeSingle != None):
                qrString = str(qrCodeSingle[0], encoding='utf-8')

                if (qrString.find(self.qrCodeDefaultExtension) != -1):
                    if (oldQrCode != qrString):
                        newList = newList[0:]
                        newList.clear()
                    oldQrCode = qrString
                    if (self.qrCodeOnDocuments == 0):
                        continue

            fileListe[oldQrCode] = newList
            newList.append(file)

        return fileListe
