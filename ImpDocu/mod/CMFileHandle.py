#
# -*- coding: utf-8 -*-

import os
import time
import sys
import re
import json
import glob
import hashlib

# self made modules
sys.path.append("../mod/")

from CMConfig import CMConfig
from CMSubProcessHandle import CMSubProcessHandle

class CMFileHandle(CMConfig):
    '''


    '''
    subProcessHandle = CMSubProcessHandle()

    def __init__(self):
        '''

        '''


    def readTheMetaDataFromFile(self, files):
        '''

        :param files:
        :return:
        '''
        metaData = ""
        try:
            for file in files:
                file = file + ".txt"
                if (os.path.exists(file)):
                    self.loggerHandle.messages("d", "Read the meta data from file: {}".format(file))
                    readObj = open(file, "r+")
                    content = readObj.readlines()
                    for line in content:
                        line = line.strip()
                        metaData += line + " "

        except IOError as err:
            self.loggerHandle.messages("e", "We have a i/o exception error with read the meta file: {}, error: {}".format(file, err))

        except Exception as err:
            self.loggerHandle.messages("e", "We have a exception error with read the meta file: {}, error: {}".format(file, err))

        return metaData


    def readIndexingInformationFromFileHeader(self, filename):
        '''

        :param filename:
        :return:
        '''
        content = None
        fileDict = None
        try:
            if (os.path.exists(filename)):
                self.loggerHandle.messages("d", "Read the meta data from file: {}".format(filename))
                readObj = open(filename, "r+b")
                for line in readObj.readlines():
                    line = line.decode()

                    if re.match('{.*_IPDC_.*}', line, re.IGNORECASE) is not None:
                        line = line.strip()
                        content = line

                    if re.match('%PDF', line, re.IGNORECASE) is not None:
                        break

                if (content != None):
                    fileDict = json.loads(content)

        except IOError as err:
            self.loggerHandle.messages("e", "We have a i/o exception error with read from file: {}, error: {}".format(filename, err))

        except Exception as err:
            self.loggerHandle.messages("e", "We have a exception error with read from file: {}, error: {}".format(filename, err))

        return fileDict


    def insertIndexingInformationIntoFileHeader(self, fileDict):
        '''

        :param fileDict:
        :return:
        '''
        tmpFileDict = {}

        for file in sorted(fileDict):
            try:
                if (os.path.exists(fileDict[file]['documentLink'])):
                    self.loggerHandle.messages("d","Insert the index information into file: {}".format(fileDict[file]['documentLink']))
                    writeObj = open(fileDict[file]['documentLink'], "r+b")
                    content = writeObj.read()

                    writeObj = open(fileDict[file]['documentLink'], "w+")
                    tmpFileDict[file] = fileDict[file]
                    string = json.dumps(tmpFileDict)
                    tmpFileDict.clear()
                    writeObj.write(string+"\n")
                    writeObj.close()

                    writeObj = open(fileDict[file]['documentLink'], "a+b")
                    writeObj.write(content)
                    writeObj.close()

                    fileDict[file].update({'documentIndexed': '1'})

                else:
                    self.loggerHandle.messages("w","File not found: {}".format(fileDict[file]['documentLink']),)

            except IOError as err:
                self.loggerHandle.messages("e", "We have a i/o exception error with write into file: {}, error: {}".format(file, err))

            except Exception as err:
                self.loggerHandle.messages("e", "We have a exception error with write into file: {}, error: {}".format(file, err))

        return fileDict


    def makeDirFromProcessID(self, path):
        '''

        :param directory:
        :return:
        '''

        path = path + self.loggerHandle.pid
        success = self.createFoldersByName(path)

        return success


    def sortKeyFunc(self, string):
        '''
        this function sort filenames with integers from os.path.basename
        :param s:
        :return:
        '''
        return int(os.path.basename(string)[:-4])

    def getFileExists(self, filename):
        '''

        :param filename:
        :return:
        '''
        success = False
        try:
            if (os.path.isfile(filename)):
                success = True

            else:
                success = False

        except IOError as error:
            self.loggerHandle.messages("e", "We have a i/o exception error with test if file: {} exists, error: {}".format(filename, error))
            success = False

        except Exception as error:
            self.loggerHandle.messages("e", "We have a exception error with test if file: {} exists, error: {}".format(filename, error))
            success = False

        return success


    def checkDirectoryIsEmptyAndRemove(self, pathList):
        '''

        :param pathList:
        :return:
        '''
        for path in pathList:
            self.removeDirectory(path)
            self.loggerHandle.messages("e","Following directory will be cleaned from empty directories:{}".format(path))



    def removeDirectory(self, path):
        '''

        :return:
        '''
        success = False
        try:
            if (os.path.exists(path) and os.path.isdir(path)):
                files = os.listdir(path)
                if len(files):
                    for f in files:
                        fullpath = os.path.join(path, f)
                        if os.path.isdir(fullpath):
                            self.removeDirectory(fullpath)

                # if folder empty, delete it
                files = os.listdir(path)
                if len(files) == 0:
                    self.loggerHandle.messages("d","Follow directory will be remove, because there is empty: {}".format(path))
                    os.rmdir(path)
                    success = True

        except IOError as error:
            self.loggerHandle.messages("e", "We have a i/o exception error with remove the directory: {}, error: {}".format(path, error))
            success = False

        except Exception as error:
            self.loggerHandle.messages("e", "We have a exception error with remove the directory: {}, error: {}".format(path, error))
            success = False

        return success


    def getCreationDateFromFile(self, filename):
        '''

        :param filename:
        :return:
        '''
        lastModify = None

        try:
            creationDate = os.stat(filename)
            creationDate = creationDate.st_ctime

        except IOError as error:
            self.loggerHandle.messages("e", "We have a i/o exception error with read the creation time from file: {}, error: {}".format(filename, error))
            creationDate = None

        except Exception as error:
            self.loggerHandle.messages("e", "We have a exception error with read the creation time from file: {}, error: {}".format(filename, error))
            creationDate = None

        return creationDate


    def getLastModifiyFromFile(self, filename):
        '''

        :param filename:
        :return:
        '''
        lastModify = None

        try:
            lastModify = os.stat(filename)
            lastModify = lastModify.st_mtime

        except IOError as error:
            self.loggerHandle.messages("e", "We have a i/o exception error with read the last modify time from file: {}, error: {}".format(filename, error))
            lastModify = None

        except Exception as error:
            self.loggerHandle.messages("e", "We have a exception error with read the last modify time from file: {}, error: {}".format(filename, error))
            lastModify = None

        return lastModify


    def getFileSizeFromFile(self, filename):
        '''

        :param filename:
        :return:
        '''
        filesize = None

        try:
            statinfo = os.stat(filename)
            filesize = statinfo.st_size

        except IOError as error:
            self.loggerHandle.messages("e", "We have a i/o exception error with read the file size from file: {}, error: {}".format(filename, error))
            filesize = None

        except Exception as error:
            self.loggerHandle.messages("e", "We have a exception error with read the file size from file: {}, error: {}".format(filename, error))
            filesize = None

        return filesize



    def getAllFilesFromDirectory(self, directory):
        '''

        :param directory:
        :return:
        '''
        allFiles = []

        try:
            allFiles = glob.glob(directory+"*.*")

        except Exception as error:
            self.loggerHandle.messages("e", "We have a exception error with read the directory: {}, error: {}".format(directory,error))

        return allFiles


    def getFilesFromDirectoryWithExtension(self, directory, extension):
        '''

        :param directory:
        :return:
        '''
        allFiles = []

        try:
            allFiles = glob.glob(directory+"*." + extension)

        except Exception as error:
            self.loggerHandle.messages("e", "We have a exception error with read the directory: {}, error: {}".format(directory, error))

        return allFiles


    def getMD5ValueFromFileList(self, fileList):
        '''

        :param fileList:
        :return:
        '''
        for file in fileList:
            try:
                if(os.path.exists(fileList[file]['documentLink'])):
                    md5Value = self.getMD5ValueFromFile(fileList[file]['documentLink'])
                    fileList[file].update({'documentMD5Sum': md5Value})

            except IOError as err:
                self.loggerHandle.messages("e","We have a i/o exception error get the md5 value for file: {}, error: {}".format(file, err))

            except Exception as err:
                self.loggerHandle.messages("e","We have a exception error get the md5 value for file: {}, error: {}".format(file, err))

        return fileList


    def getMD5ValueFromFile(self,filename,block_size=2**20):
        '''

        :param filename:
        :return:
        '''
        md5Value = hashlib.md5()
        try:
            file = open(filename, 'rb')
            while True:
                data = file.read(block_size)
                if not data:
                    break

            md5Value.update(data)

        except IOError as err:
            self.loggerHandle.messages("e","We have a i/o exception error with get the md5 value for file: {}, error: {}".format(filename,err))
            md5Value = None

        except Exception as err:
            self.loggerHandle.messages("e", "We have a exception error get the md5 value for file: {}, error: {}".format(filename,err))
            md5Value = None

        return md5Value.hexdigest()


    def moveAllFilesToDateDirectory(self, workingDirectories, date):
        '''

        :param workingDirectories:
        :return:
        '''
        for path in workingDirectories:
            if(path.find(date) != -1):
                oldPath = path.replace(date, '')
                for file in self.getAllFilesFromDirectory(oldPath):
                    try:
                        newFilePath = path + "/" + os.path.split(file)[1]
                        os.rename(file, newFilePath)

                    except IOError as error:
                        self.loggerHandle.messages("e","We have a i/o exception error with move the file: {}, error: {}".format(file, error))

                    except Exception as error:
                        self.loggerHandle.messages("e","We have a exception error with move the file: {}, error: {}".format(file, error))


    def createFoldersByName(self, directory):
        '''

        :param directory:
        :return:
        '''
        success = False
        try:
            if (os.path.exists(directory)):
                self.loggerHandle.messages("d","The directoy {} already exists.".format(directory))
            else:
                os.mkdir(directory)
            success = True
        except IOError as error:
            self.loggerHandle.messages("e", "We have a i/o exception error with create folder: {}, error: {}".format(directory, error))
            success = False

        except Exception as error:
            self.loggerHandle.messages("e", "We have a exception error with create folder: {}, error: {}".format(directory, error))
            success = False

        return success


    def createFoldersFromList(self, directory):
        '''

        :param directory:
        :return:
        '''
        success = False
        countGood = 0
        countFalse = 0

        for path in directory:
            success = self.createFoldersByName(path)

            if (success != True):
                countFalse += 1
            else:
                countGood += 1


    def removeFilesFromDirectoryWithExtension(self, directory, extension):
        '''

        :param directory:
        :param extention:
        :return:
        '''

        for file in glob.glob(directory + "*." + extension):
            self.removeFileFromDirectory(file)


    def moveFileFromTo(self, fromDirectory, toDirectory):
        '''

        :param fromDirectory:
        :param toDirectory:
        :param extension:
        :return:
        '''
        success = False

        try:
            os.rename(fromDirectory, toDirectory)

            success = True

        except IOError as err:
            self.loggerHandle.messages("e", "We have a i/o exception error with move file: {}, error: {}".format(fromDirectory, err))
            success = False

        except Exception as err:
            self.loggerHandle.messages("e", "We have a exception error with move file: {}, error: {}".format(fromDirectory, err))
            success = False

        return success


    def moveFilesFromDirectoryWithExtension(self, fromDirectory, toDirectory, extension):
        '''

        :param fromDirectory:
        :param toDirectory:
        :param extension:
        :return:
        '''

        for file in glob.glob(fromDirectory + "*."+extension):
            newFile = toDirectory + os.path.splitext(os.path.basename(file))[0] + "." + extension
            try:
                os.rename(file, newFile)

            except IOError as err:
                self.loggerHandle.messages("e", "We have a i/o exception error with move file: {}, error: {}".format(file, err))

            except Exception as err:
                self.loggerHandle.messages("e", "We have a exception error with move file: {}, error: {}".format(file, err))


    def removeAllFilesFromDirectory(self, directory):
        '''

        :param directory:
        :return:
        '''
        success = False
        countGood = 0
        countFalse = 0

        for file in glob.glob(directory+"*"):
            success = self.removeFileFromDirectory(file)

            if (success != True):
                self.loggerHandle.messages ("e", "File: {} was not successfully removed!".format(file))
                countFalse += 1
            else:
                countGood += 1


    def removeFileFromDirectory(self, file):
        '''

        :param directory:
        :return:
        '''
        success = False

        try:
            if (os.path.isfile(file)):
                os.remove(file)

            success = True
        except IOError as error:
            self.loggerHandle.messages("e", "We have a i/o exception with remove file: {}, error: {}".format(file, error))
            success = False

        except Exception as error:
            self.loggerHandle.messages("e", "We have a exception with remove file: {}, error: {}".format(file, error))
            success = False

        return success


    def makeNewFileIfOverFileSize(self, file, fileSize):
        '''

        :param file:
        :param fileSize:
        :return:
        '''
        # Check file size - copy away if reached max size
        if os.path.exists(self.logfile):
            fileInfo = os.stat(self.logfile)
            if fileInfo.st_size > self.logFileMaxSize:
                newFileName = self.logfile + "_" + time.strftime("%Y%m%d_%H%M")
                os.rename(self.logfile, newFileName)


    def deleteFilesFromList(self, fileList):
        '''

        :param fileList:
        :return:
        '''

        for file in fileList:
            try:
                os.remove(file)

            except IOError as error:
                self.loggerHandle.messages("e", "We have a error with delete file: {}, error: {}".format(file,error))
            except Exception as error:
                self.loggerHandle.messages("e", "We have a error with delete file: {}, error: {}".format(file, error))

