########! /usr/bin/python
#
# -*- coding: utf-8 -*-
import os
import subprocess
import time
import sys
#from queue import Queue
import threading
import re
import json
import glob

from PIL import Image
import pyqrcode
import zbarlight
import sane
import numpy


class CMConvertImage:
    '''

    '''

    convert = "/usr/bin/convert"
    convertInputDirectory = "../inc/"
    convertOutputDirectory = "../tmp/"

    def __init__(self):
        '''
        
        '''
        #self.convertDirectory = self.getConvertPath()
        #self.convert = self.getConvertPathFromExecutable()


    def setConvertPath(self, inputDirectory, outputDirectory):
        '''

        :return:
        '''

        self.convertinputDirectory = inputDirectory
        self.convertOutputDirectory = outputDirectory


    def getConvertInputPath(self):
        '''

        :return:
        '''

        return self.convertInputDirectory


    def getConvertOutputPath(self):
        '''

        :return:
        '''

        return self.convertOutputDirectory


    def getConvertPathFromExecutable(self):
        '''

        :return:
        '''
        convertProcess = []
        convertProcess.append("which")
        convertProcess.append("convert")
        success = False
        tmp = None

        # now we make a subprocess
        check = subprocess.Popen(convertProcess, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        check.wait()

        if (check.returncode == 0):
            tmp = check.stdout.readline()
            tmp = str(tmp, encoding=('utf-8')).strip()

        return tmp


    def convertAllFilesInDirectory(self):
        '''

        :return:
        '''

        for file in glob.glob(self.convertInputDirectory+"*.jpg"):
            self.pic2GraysSale(file)


    def pic2EPSFile(self, filename):
        '''

        :param filename:
        :return:
        '''

        fileNameOutput = filename.replace(".png", '.eps')

        command = ("{} \"{}\" \"{}\"".format(self.convert, filename, fileNameOutput))

        try:
            check = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            check.wait()

            if (check.returncode == 0):
                pass
            elif (check.returncode == 1):
                pass
            elif (check.returncode == 256):
                pass

        except IOError as error:
            pass

        except Exception as error:
            pass


    def pic2GraysSale(self, filename):
        '''
        '''
        grayImage = ""
        #img = Image.open(filename).convert('LA')
        #grayImage = filename + "gray"
        #img.save(grayImage)
        filename = filename.replace("_gray.jpg", '')
        fileNameOutputPath = os.path.splitext(os.path.basename(filename))[0]
        fileNameOutput = self.convertOutputDirectory + fileNameOutputPath
        fileNameOutput = fileNameOutput.replace('.jpg', '')
        fileNameOutput = fileNameOutput+"_gray.jpg"

        # convert -colorspace Gray out2.jpg check.jpg
        command = ("{} -colorspace Gray \"{}\" \"{}\"".format(self.convert, filename, fileNameOutput))

        try:
            check = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            check.wait()

            if (check.returncode == 0):
                pass
            elif (check.returncode == 1):
                pass
            elif (check.returncode == 256):
                pass

        except IOError as error:
            pass

        except Exception as error:
            pass
