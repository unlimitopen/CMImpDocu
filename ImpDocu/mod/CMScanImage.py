########! /usr/bin/python
#
# -*- coding: utf-8 -*-

import os
import subprocess
import time
import sys
#from queue import Queue
import threading
import re
import json

from PIL import Image
import pyqrcode
import zbarlight
import sane
import numpy

#
# this sitations must be corrected:
# if you make a instance, then they have a problem with scanImageGetPathFromExecutable
# we need everytime from subprocess a try except block ....
#

class CMScanImage:
    '''

    '''
    scanFladbed = 'Flatbed'
    scanADF = 'ADF'
    scanDeviceCount = 1
    scanDocumentDuplex = 0
    scanCount = 50
    scanSource = "ADF"
    scanXCordinate = 211
    scanYCordinate = 295
    scanResolution = 200
    scanDuplex = 0
    scanDevice = 'hpaio:/net/officejet_pro_8600?ip=192.168.22.32'

    scanOptions = {'scancount': scanCount, 'xcordinate': scanXCordinate, 'ycordinate': scanYCordinate, 'scansource': scanSource, \
                   'scanresolution': 200, 'scandevice': scanDevice, 'scanduplex': scanDuplex, }

    scanImage = "/usr/bin/scanimage"
    scanImageVersion = "1.0.25"
    scanImageDirectoryPath = "../inc/"
    scanDeviceList = { scanDeviceCount: scanOptions,}

    def __init__(self):
        '''

        :return:
        '''
        self.scanImageDirectoryPath = self.getScanImagePath()
        self.scanImageVersion = self.scanImageGetVersionFromExecutable()
        self.scanImage = self.scanImageGetPathFromExecutable()

        success = False


    def setScanDeviceList(self, scanOptions):
        '''
        '''
        pass


    def setScanYCordinate(self, yCordinate):
        '''

        :param yCordinate:
        :return:
        '''
        self.scanYCordinate = yCordinate


    def getScanYCordinate(self):
        '''

        :return:
        '''
        return self.scanYCordinate


    def setScanXCordinate(self, xCordinate):
        '''

        :param xCordinate:
        :return:
        '''
        self.scanXCordinate = xCordinate


    def getScanXCordinate(self):
        '''

        :return:
        '''
        return self.scanXCordinate


    def setScanSource(self, scanSource):
        '''

        :param scanSource:
        :return:
        '''
        self.scanOptions['scansource'] = scanSource


    def getScanSource(self):
        '''

        :return:
        '''
        return self.scanOptions['scansource']


    def setScanDevice(self, scanDevice):
        '''

        :param scanDevice:
        :return:
        '''
        self.scanDevice = scanDevice


    def getScanDevice(self):
        '''

        :return:
        '''
        return self.scanDevice


    def setScanResolution(self, resolution):
        '''

        :param resolution:
        :return:
        '''

        self.scanResolution = resolution


    def getScanResolution(self):
        '''

        :return:
        '''
        return self.scanResolution


    def setScanImagePath(self, directory):
        '''

        :param directory:
        :return:
        '''

        self.scanImageDirectoryPath = directory


    def getScanImagePath(self):
        '''

        :return:
        '''

        return self.scanImageDirectoryPath



    def scanImageGetVersion(self):
        '''

        :return:
        '''
        return self.scanImageVersion


    def scanImageGetPathFromExecutable(self):
        '''

        :return:
        '''

        scanProcess = []
        scanProcess.append("which")
        scanProcess.append("scanimage")
        success = False
        tmp = None

        # now we make a subprocess
        check = subprocess.Popen(scanProcess, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        check.wait()

        if (check.returncode == 0):
            tmp = check.stdout.readline()
            tmp = str(tmp, encoding=('utf-8')).strip()

        return tmp


    def scanImageGetVersionFromExecutable(self):
        '''

        :return:
        '''
        scanProcess = []
        scanProcess.append(self.scanImage)
        scanProcess.append("--version")
        success = False
        tmp = None

        # now we make a subprocess
        check = subprocess.Popen(scanProcess, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        check.wait()

        if (check.returncode == 0):
            tmp = check.stdout.readline()
            tmp = str(tmp, ('utf-8')).strip()
            tmp = tmp.split(" ")
            tmp = str(tmp[-1:])
            tmp = tmp.replace('[',"")
            tmp = tmp.replace(']',"")
            tmp = tmp.replace('\'',"")

        return tmp


    def scanImageGetDeviceOptions(self):
        '''

        :return:
        '''
        scanProcess = []
        scanProcess.append(self.scanImage)
        scanProcess.append("-A")
        scanProcess.append("--device")
        scanProcess.append(self.scanOptions['scandevice'])
        success = False

        # now we make a subprocess
        check = subprocess.Popen(scanProcess, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        check.wait()

        if (check.returncode == 0):
            for line in check.stdout.readlines():
                    line = str(line, encoding='utf-8')
                    if (line.find('All options specific to device') != -1):
                        success = True

                    if (success == True):
                        print (line)



    def getScanImage(self):
        '''
        
        :return: 
        '''
        #scanImageFile = self.scanImageDirectoryPath + "scanImage.jpg"
        returncode = 1
        success = False
        command = ("{} --batch={}%d.tif --batch-count {} -x {} -y {} --mode Color --source {} --resolution {} --format=tiff --device {}"
                   .format(self.scanImage, self.scanImageDirectoryPath, self.scanOptions['scancount'], self.scanOptions['xcordinate'], self.scanOptions['ycordinate'], \
                           self.scanOptions['scansource'], self.scanOptions['scanresolution'], self.scanOptions['scandevice']))

        if (self.scanDocumentDuplex == 1 and self.scanOptions['scanduplex'] == 0 ):
            firstScan = 0
            self.setScanSource(self.scanFladbed)
            for scnt in range(1,self.scanCount):
                command = (
                    "{} -x {} -y {} --progress --mode Color --source {} --resolution {} --format=tiff --device {} >{}{}.tif"
                        .format(self.scanImage, self.scanOptions['xcordinate'], self.scanOptions['ycordinate'], \
                                self.scanOptions['scansource'], self.scanOptions['scanresolution'],
                                self.scanOptions['scandevice'], self.scanImageDirectoryPath, scnt))

                try:
                    if (firstScan != 0):

                        print("If you want to scan more pages, please press <ENTER>")
                        print("If you want to stop scan, please press <q>")
                    else:
                        print("Please press <ENTER> to begin the first scan")

                    firstScan = 1
                    eingabe = str(input())

                    if (eingabe == ""):
                        returncode, output = self.runSubprocessCommand(command)

                    elif (eingabe == "q"):
                        break

                except SyntaxError as err:
                    print(err)

                except Exception as err:
                    print(err)

        else:
            returncode, output = self.runSubprocessCommand(command)

        if(returncode == 0):
            success = True

        return success


    def run_command(self, command):
        process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        while True:
#            output = process.stdout.readline()
            output = process.stdout.readline().decode()
            if output == '' and process.poll() is not None:
                break
            if output:
                print( output.strip())
        rc = process.poll()
        return rc


    def runSubprocessCommand(self, command):
        '''

        :param command:
        :return:
        '''

        try:
            process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            process.wait()
            outputProcess = process.stdout.readlines()

            if (process.returncode == 0):
                success = True
            elif (process.returncode == 1):
                print ("scanner is not aviable, please check first")
            elif (process.returncode == 7):
                print ("scanner has lower scaninput as defined, please look your scans")
            elif (process.returncode == 256):
                print("Error, scanimage file will not found")

        except IOError as error:
            print ("we have an i/o error here:".format(error))

        except Exception as error:
            print ("something went wrong: {}".format(error))

        return process.returncode, outputProcess


