########! /usr/bin/python
#
# -*- coding: utf-8 -*-

import os
import subprocess
import time
import sys
#from queue import Queue
import threading
import re
import json
import threading
from queue import Queue
import time

class CMTesseractHandle():
    '''


    '''
    ocrOptions = {'language': 'deu', 'outputtype1': 'pdf', 'outputtype2': 'txt'}
    ocrTesseract = "/usr/bin/tesseract"
    ocrTesseractVersion = "3.04.01"
    ocrThreads = 2

    def __init__(self):
        '''

        '''

        pass

    def ocrTesseractGetVersion(self):
        '''

        :return:
        '''
        return self.ocrTesseractVersion


    def ocrTesseractGetPathFromExecutable(self):
        '''

        :return:
        '''
        tesseractProcessCmd = []
        tesseractProcessCmd.append("which")
        tesseractProcessCmd.append("tesseract")
        success = False
        tmp = None

        # now we make a subprocess
        tesseractProcess = subprocess.Popen(tesseractProcessCmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        tesseractProcess.wait()

        if (tesseractProcess.returncode == 0):
            tmp = tesseractProcess.stdout.readline()
            tmp = str(tmp, encoding=('utf-8')).strip()

        return tmp


    def makeOcrTesseractFromImage(self, inputFile, outputFile):
        '''

        :return:
        '''
        success = False

        #while True:

        command = ("{} -l {} {} {} {} {}".format(self.ocrTesseract, self.ocrOptions['language'], inputFile, outputFile, \
                                              self.ocrOptions['outputtype1'], self.ocrOptions['outputtype2']))
        try:
            tesseractProcess = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            tesseractProcess.wait()

            if (tesseractProcess.returncode == 0):
                success = True
            elif (tesseractProcess.returncode == 1):
                success = False
                pass
            elif (tesseractProcess.returncode == 256):
                success = False
                pass

        except IOError as error:
            success = False
            pass


        except Exception as error:
            success = False
            pass

        #queue.task_done()

        return success


    def makeTesseractThreads(self, fileListe, fileTime, tmpDirectory):
        '''

        :return:
        '''

        # Create the queue and threader
        q = Queue()

        ###########
        count = 0
        newFileList = {}
        newFiles = []
        # at here we can use threading
        for k in fileListe.keys():
            for file in fileListe[k]:
                newFilename = tmpDirectory + fileTime + "_" + k + "_FILE_" + str(count)

                self.makeOcrTesseractFromImage(file, newFilename)

                newFiles.append(newFilename)
                count += 1

            newFileList[k] = newFiles
            newFiles = newFiles[0:]
            newFiles.clear()
            count = 0

        return newFileList

        #############

        #for file in fileListe:
        #    # how many threads are we going to allow for
        #    for x in self.ocrThreads:
        #        t = threading.Thread(target=self.makeOcrTesseractFromImage())

        #        # classifying as a daemon, so they will die when the main dies
        #        t.daemon = True

        #        # begins, must come after daemon definition
        #        t.start()

        #    start = time.time()

        #   # 20 jobs assigned.
        #    for worker in range(20):
        #        q.put(worker)

        #    # wait until the thread terminates.
        #    q.join()


