#!/usr/bin/env python
# coding: utf-8

import os
import subprocess
import time
import sys
#from queue import Queue
import threading
import re
import json
import glob
import datetime

# self made modules
sys.path.append("../mod/")

from CMLogger import CMLogger
from CMFileHandle import CMFileHandle

class CMParser():
    '''

    '''
    workingDirectory = '../tmp/'
    workingExtension = 'txt'
    fileHandle = CMFileHandle()
    loggerHandle = CMLogger()
    loggerHandle.setLogFile('../log/impDocu.log')

    ownerIdentificationList = ['Paul Paul','Henry Henry','Jacky Jacky',]

    documentIdentificationList = ['Billing',
                                  'Insurance',]

    companyIdentificationList = ['Company1','Company2',]

    dateIdentificationList = ['(?:Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)\s+[0-2][0-1][0-5][0-9]',
                              '[0-3][0-9]\.[0-1][0-9]\.[0-2][0-1][0-5][0-9]',
                              '[1-3][0-9]_[0-1][0-9]_[0-2][0-1][0-5][0-9]',
                              '[0-3][0-9].\s+(?:Jan|Feb|Mär|Apr|Mai|Jun|Jul|Aug|Sep|Okt|Nov|Dez)\.\s+[0-2][0-1][0-5][0-9]',
                              '[0-3][0-9].\s+(?:Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)\s+[0-2][0-1][0-5][0-9]']

    def __init__(self):
        '''

        '''
        pass


    def setOwnerIdentificationList(self, owner):
        '''

        :param owner:
        :return:
        '''

        self.ownerIdentificationList = owner


    def getOwnerIdentificationList(self):
        '''

        :param owner:
        :return:
        '''

        return self.ownerIdentificationList


    def setWorkingDirectory(self, directory):
        '''

        :param directory:
        :return:
        '''
        self.workingDirectory = directory


    def getWorkingDirectory(self):
        '''

        :return:
        '''

        return self.workingDirectory


    def setWorkingExtensions(self, extension):
        '''

        :return:
        '''
        self.workingExtension = extension


    def getWorkingExtensions(self):
        '''

        :return:
        '''

        return self.workingExtension


    def getDateIdentificationList(self):
        '''

        :return:
        '''

        return self.dateIdentificationList


    def setDateIdentificationList(self):
        '''

        :return:
        '''
        pass


    def getDocumentIdentificationList(self):
        '''

        :return:
        '''
        return self.documentIdentificationList


    def setDocumentIdentificationList(self):
        '''

        :return:
        '''
        pass


    def getCompanyIdentificationList(self):
        '''

        :return:
        '''
        return self.companyIdentificationList


    def setCompanyIdentificationList(self):
        '''

        :return:
        '''
        pass


    def getContentFromFile(self, content, ):
        '''

        :param file:
        :return:
        '''

        pass

    def getIdentificationsFromDocumentPath(self, directory):
        '''

        :param directory:
        :return:
        '''

        files = self.fileHandle.getFilesFromDirectoryWithExtension(self.getWorkingDirectory(), self.getWorkingExtensions())

        for file in files:
            print(file)


    def getIdentificationsFromDocument(self, filename):
        '''

        :param filename:
        :return:
        '''

        values = {}
        values = self.getOwnerIdentification(filename)
        values.update(self.getCompanyIdentification(filename))
        values.update(self.getDocumentIdentification(filename))
        values.update(self.getDateIdentification(filename))

        return values


    def getOwnerIdentification(self, filename):
        '''

        :param filename:
        :return:
        '''

        success = False
        dateIdentification = None
        searchValueDatePos = 0
        valuesFromDocument = {}
        searchValueDate = self.getRegularExpression(self.getOwnerIdentificationList())

        dateIdentification, searchValueDatePos = self.searchContent(filename, searchValueDate)

        valuesFromDocument['owner'] = dateIdentification
        valuesFromDocument['ownerpos'] = searchValueDatePos

        if (valuesFromDocument['owner'] != None and valuesFromDocument['ownerpos'] != 0):
            self.loggerHandle.messages("d", "we have found in the file: {} the date element: {} on position: {}".format(filename,valuesFromDocument['owner'],valuesFromDocument['ownerpos']))
            valuesFromDocument['ownerStatus'] = True
        else:
            self.loggerHandle.messages("w", "we have not found in the file: {} the date element: {}".format(filename,searchValueDate))
            valuesFromDocument['ownerStatus'] = False

        return valuesFromDocument


    def getDateIdentification(self, filename):
        '''

        :param filename:
        :return:
        '''

        success = False
        ownerIdentification = None
        searchValueDatePos = 0
        valuesFromDocument = {}
        searchValueDate = self.getRegularExpression(self.getDateIdentificationList())

        dateIdentification, searchValueDatePos = self.searchContent(filename, searchValueDate)

        valuesFromDocument['date'] = dateIdentification
        valuesFromDocument['datepos'] = searchValueDatePos

        if (valuesFromDocument['date'] != None and valuesFromDocument['datepos'] != 0):
            self.loggerHandle.messages("d", "we have found in the file: {} the date element: {} on position: {}".format(filename,valuesFromDocument['date'],valuesFromDocument['datepos']))
            valuesFromDocument['dateStatus'] = True
        else:
            self.loggerHandle.messages("w", "we have not found in the file: {} the date element: {}".format(filename,searchValueDate))
            valuesFromDocument['dateStatus'] = False

        return valuesFromDocument


    def getCompanyIdentification(self, filename):
        '''

        :param filename:
        :return:
        '''

        success = False
        compayIdentification = None
        searchValueCompanyPos = 0
        valuesFromDocument = {}
        searchValueCompany = self.getRegularExpression(self.getCompanyIdentificationList())

        compayIdentification, searchValueCompanyPos = self.searchContent(filename, searchValueCompany)

        valuesFromDocument['company'] = compayIdentification
        valuesFromDocument['companypos'] = searchValueCompanyPos

        if (valuesFromDocument['company'] != None and valuesFromDocument['companypos'] != 0):
            self.loggerHandle.messages("d", "we have found in the file: {} the company element: {} on position: {}".format(filename,valuesFromDocument['company'],valuesFromDocument['companypos']))
            valuesFromDocument['companyStatus'] = True
        else:
            self.loggerHandle.messages("w", "we have not found in the file: {} the company element: {}".format(filename,searchValueCompany))
            valuesFromDocument['companyStatus'] = False

        return valuesFromDocument


    def getDocumentIdentification(self, filename):
        '''

        :param filename:
        :return:
        '''

        success = False
        documentIdentification = None
        searchValueDocumentPos = 0
        valuesFromDocument = {}
        searchValueDocument = self.getRegularExpression(self.getDocumentIdentificationList())

        documentIdentification, searchValueDocumentPos = self.searchContent(filename, searchValueDocument)
        valuesFromDocument['document'] = documentIdentification
        valuesFromDocument['documentpos'] = searchValueDocumentPos

        if (valuesFromDocument['document'] != None and valuesFromDocument['documentpos'] != 0 ):
            self.loggerHandle.messages("d", "we have found in the file: {} the document type element: {} on position: {}".format(filename, valuesFromDocument['document'], valuesFromDocument['documentpos']))
            valuesFromDocument['documentStatus'] = True

        else:
            self.loggerHandle.messages("w", "we have not found in the file: {} the document type element: {}".format(filename, searchValueDocument))
            valuesFromDocument['documentStatus'] = False

        return valuesFromDocument


    def searchContent(self, filename, searchContent):
        '''

        :param filename:
        :param searchContent:
        :return:
        '''

        searchContentValue = None
        posCounter = 0

        if (os.path.exists(filename) == True):

            try:
                fobj = open(filename, "r")
                line = fobj.readlines()

            except IOError as err:
                self.loggerHandle.messages("e", "we have a problem with the file: {} with error: {}".format(filename, err))

            except Exception as err:
                self.loggerHandle.messages("e", "we have a problem with the file: {} with error: {}".format(filename, err))

            posCounter = 0
            for li in line:
                li.strip()
                li = li.lower()

                if (len(li) <= 2):
                    continue

                posCounter += 1
                
                if re.match(searchContent, li, re.IGNORECASE) is not None:
                    l = re.findall(searchContent,li, re.IGNORECASE)
                    searchContentValue = l
                    break

            fobj.close

            return searchContentValue, posCounter


    def getRegularExpression(self, regularExpressionList):
        '''

        :return:
        '''
        regularExpression = ".+?("

        for items in regularExpressionList:
            regularExpression += items + "|"

        regularExpression = regularExpression[:-1]
        regularExpression += ")"

        return regularExpression


    def testRegularExpressions(self):
        '''

        :return:
        '''

        string = 'Company1 23. Jan. 2017 Billing Januar 2017 Insurance 19_10_2017,23.09.2016, 44.12.2017 05.12.2017, 42_99_0001, 23_22_1111, 28_05_2017'

        newident = self.getRegularExpression(self.getCompanyIdentificationList())
        newdateident = self.getRegularExpression(self.getDateIdentificationList())
        newdocumentident = self.getRegularExpression(self.getDocumentIdentificationList())

        if re.match(newident, string, re.IGNORECASE) is not None:
            l = re.findall(newident, string, re.IGNORECASE)
            print(l)

        if re.match(newdocumentident, string, re.IGNORECASE) is not None:
            l = re.findall(newdocumentident, string, re.IGNORECASE)
            print(l)

        if re.match(newdateident, string, re.IGNORECASE) is not None:
            x = re.findall(newdateident, string, re.IGNORECASE)
            print(x)




if __name__ == "__main__":
    parser = CMParser()
    # check the module directly
    for file in glob.glob('../tmp/*.txt'):
        parser.getIdentificationsFromDocument(file)

