########! /usr/bin/python
#
# -*- coding: utf-8 -*-

import os
import subprocess
import time
import sys
#from queue import Queue
import threading
import re
import json


class CMTimeHandle():
    '''

    '''

    def __init__(self):
        '''

        '''
        pass

    def getDateFromTimeAndDate(self, timeNow):
        '''

        :param timeNow:
        :return:
        '''
        date, time = timeNow.split(" ")

        return date


    def getTimeFromDateAndTime(self, timeNow):
        '''

        :param timeNow:
        :return:
        '''
        date, time = timeNow.split(" ")

        return time


    def getTimeAndDate(self):
        '''
        return localtime string
        :return: time string (%Y-%m-%d %H:%M:%S)
        '''

        return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))