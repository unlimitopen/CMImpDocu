#
# -*- coding: utf-8 -*-

import os
import subprocess
import time
import sys
#from queue import Queue
import threading
import re
import json

# self made modules
sys.path.append("../mod/")

from CMConfig import CMConfig
from CMFileHandle import CMFileHandle
from CMTimeHandle import CMTimeHandle

class CMGhostscriptHandle(CMConfig):
    '''

    '''
    fileHandle = CMFileHandle()
    timeHandle = CMTimeHandle()

    def __init__(self):
        '''

        '''


    def makeGsHandleToMerge(self, fileDict, archiveDirectory, fileTime):
        '''

        :param newFileList:
        :return:
        '''

        # score if tesseract is ready, we need to put some pdf - files together!
        for qrCode in fileDict.keys():
            archivFilename = archiveDirectory +fileTime+"/"+fileTime + "_" + qrCode + "_FILE.pdf"

            if(self.fileHandle.getFileExists(archivFilename) == False):

                self.loggerHandle.messages("d","Following files will be merged: {}".format(qrCode))

                if (len(fileDict[qrCode]['tmpDocuments']) >= 2):
                    self.gsHandleToMergePDFFile(archivFilename, fileDict[qrCode]['tmpDocuments'])

                else:
                    self.fileHandle.moveFileFromTo(fileDict[qrCode]['tmpDocuments'][0]+".pdf",archivFilename)

                fileDict[qrCode].update({'documentLink': archivFilename})
                fileDict[qrCode].update({'documentName': os.path.basename(archivFilename)})
                fileDict[qrCode].update({'documentPages': len(fileDict[qrCode]['tmpDocuments'])})
                fileSize = self.fileHandle.getFileSizeFromFile(archivFilename)
                fileDict[qrCode].update({'documentSize': fileSize})

                createDateFile = self.fileHandle.getCreationDateFromFile(archivFilename)
                createDateFile = self.timeHandle.getDateFromTimeTicksForFiles(createDateFile)
                creationTime = self.timeHandle.getTimeFromDateAndTime(createDateFile)
                creationDate = self.timeHandle.getDateFromTimeAndDate(createDateFile)

                fileDict[qrCode].update({'documentCreationDate': creationDate})
                fileDict[qrCode].update({'documentCreationTime': creationTime})

                lastModify = self.fileHandle.getLastModifiyFromFile(archivFilename)
                lastModify = self.timeHandle.getDateFromTimeTicksForFiles(lastModify)
                lastModifyDate = self.timeHandle.getDateFromTimeAndDate(lastModify)
                lastModifyTime = self.timeHandle.getTimeFromDateAndTime(lastModify)

                fileDict[qrCode].update({'documentLastModifyDate': lastModifyDate})
                fileDict[qrCode].update({'documentLastModifyTime': lastModifyTime})

            else:
                self.loggerHandle.messages("e", "The new archive file:{} already exists!".format(archivFilename))

        return fileDict


    def gsConvertPostscript2Pdf(self, filename):
        '''

        :param filename:
        :return:
        '''
        newFile = os.path.splitext(os.path.basename(filename))[0]
        newFile = '../tmp/'+newFile+".pdf"
        filename = '../tmp/'+filename
        command = 'gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=\"{}\" {}'.format(newFile, filename)

        success = False
        try:

            # FIXME !!! use the SubProcessHandle

            check = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            check.wait()
            output = check.stdout.readlines()

            if (check.returncode == 0):
                success = True
            elif (check.returncode == 1):
                print("ghostscript has a undefined error, please check first")
            elif (check.returncode == 256):
                print("Error, ghostscript file will not found")

        except IOError as error:
            print("we have an i/o error here:".format(error))

        except Exception as error:
            print("something went wrong: {}".format(error))

        return success


    def gsHandleToMergePDFFile(self,newFileName, files):
        '''

        :return:
        '''
        command = 'gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=\"{}\" '.format(newFileName,)

        for f in files:
            command += " "+f+".pdf"

        # FIXME !!! use the SubProcessHandle

        success = False
        try:
            check = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            check.wait()
            output = check.stdout.readlines()

            for lines in output:
                print (lines)

            if (check.returncode == 0):
                success = True
            elif (check.returncode == 1):
                print("ghostscript has a undefined error, please check first")
            elif (check.returncode == 256):
                print("Error, ghostscript file will not found")

        except IOError as error:
            print("we have an i/o error here:".format(error))

        except Exception as error:
            print("something went wrong: {}".format(error))

        return success


    def getTotalPagesFromPDFDocument(self, filename):
        '''

        :param filename:
        :return:
        '''
        newOutput = ""
        success = False
        command = 'gs -q -dNODISPLAY -c \"({}) (r) file runpdfbegin pdfpagecount = quit\"'.format(filename)
        try:
            check = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            check.wait()
            output = check.stdout.readlines()

            for line in output:
                line = str(line, encoding='utf-8')
                line = line.strip()
                newOutput = newOutput.join(line)

            if (check.returncode == 0):
                success = True

            elif (check.returncode == 1):
                print("ghostscript has a undefined error, please check first")
                output = 0

            elif (check.returncode == 256):
                print("Error, ghostscript file will not found")
                output = 0

        except IOError as error:
            print("we have an i/o error here:".format(error))

        except Exception as error:
            print("something went wrong: {}".format(error))

        return int(newOutput)